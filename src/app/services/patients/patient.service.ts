import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from 'src/app/utils/constants';
import { AuthService } from '../auth.service';

import { Patient } from '../../models/patient.model'; // import the patient model


const patient_url = `${BASE_URL}api/create/patient/`
const all_patients_url = `${BASE_URL}api/patient/all/`

@Injectable({
  providedIn: 'root'


})


export class PatientService {

  constructor(private http: HttpClient,private auth:AuthService) { }

  // craete patient
  create(data: any): Observable<any> {
    const {token} = this.auth.getuserdetails()
    console.log("printing token")
    console.log(token)
    console.log(data)
    return this.http.post(patient_url,{
              headers:{
              Authorization:`Bearer ${token}`,
              
          }
        }, data);
  }
  // get all patients

  getpatients(): Observable<Patient[]> {
    const {token} = this.auth.getuserdetails()
    return this.http.get<Patient[]>(all_patients_url,
      {
        headers:{
          Authorization:`Bearer ${token}`
        }
      }
      );
  }


  

 

}




// constructor(private http:HttpClient, private auth:AuthService) { }

// registerpatient(patientdetails): Observable<any>{
//   const {token} = this.auth.getuserdetails()
//   return this.http.post(this.patient_url,{
//     ...patientdetails
//   },{
//     headers:{
//       Authorization:`Bearer ${token}`
//     }
//   }).pipe(res=>res)
// }

// getpatients(): Observable<any>{
//   const {token} = this.auth.getuserdetails()
//   return this.http.get(this.all_patients_url,{
//     headers:{
//       Authorization:`Bearer ${token}`
//     }

//   }).pipe(res=>res)
// }