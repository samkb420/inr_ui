import { Component,OnInit} from '@angular/core';
import { PatientService } from 'src/app/services/patients/patient.service';
import { Patient } from 'src/app/models/patient.model';
import { AuthService } from 'src/app/services/auth.service';



@Component({
  selector: 'app-Reception-Component',
  templateUrl: './reception-component.component.html',
  styleUrls: ['./reception-component.component.css']
})


export class ReceptionComponentComponent implements OnInit{

  patient: Patient = {
    full_name: '',
    age: '',
    residence:'',
    email: '',
    phone_number: '',
    gender:''

  };

  constructor(public patientservice: PatientService) {

    
   }

  savePatient(): void {
    const data = {
      full_name: this.patient.full_name,
      age: this.patient.age,
      residence: this.patient.residence,
      email: this.patient.email,
      phone_number: this.patient.phone_number,
      gender:this.patient.gender
    };


    
    
    this.patientservice.create(data)
     
      .subscribe({
        
        next: (res) => {
          console.log(res);
          
        },
        error: (e) =>  console.error(e)
      });
  }

  newpatient(): void {
   
    this.patient = {
      full_name: '',
      age: '',
      residence:'',
      email: '',
      phone_number: '',
      gender:''
    };
  }

  ngOnInit(): void {}

 
}

